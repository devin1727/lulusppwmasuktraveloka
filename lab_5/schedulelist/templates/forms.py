from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }
    
    hari = forms.CharField(label='hari', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    tanggaldanjam = forms.DateTimeField(required=True, widget=forms.TextInput(attrs=attrs))
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs=attrs), required=True)
    kategori = forms.CharField(widget=forms.TextInput(attrs=attrs), required=True)