from django.urls import re_path
from .views import submit
from .views import show

app_name = "sched"
urlpatterns = [
    # re_path(r'^nothing/', views.nothing, name="nothing"),
    re_path(r'^', submit, name="submit"),
    re_path(r'show',show, name="show"),
]