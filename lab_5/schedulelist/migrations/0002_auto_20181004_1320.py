# Generated by Django 2.1.1 on 2018-10-04 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedulelist', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='bukan_server',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hari', models.CharField(max_length=27, verbose_name='hari')),
                ('nama_kegiatan', models.CharField(max_length=27, verbose_name='kegiatan')),
                ('kategori', models.CharField(max_length=27, verbose_name='kategori')),
            ],
        ),
        migrations.DeleteModel(
            name='Server',
        ),
    ]
