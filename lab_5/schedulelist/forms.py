from django import forms
from .models import data

class schedule_form(forms.Form):
    model = data
    fields = ['hari','waktu','nama_kegiatan','kategori']

    hari = forms.CharField(label='hari', required=True, max_length=27)
    waktu = forms.DateField(required=True)
    nama_kegiatan = forms.CharField(required=True)
    kategori = forms.CharField(required=True)

