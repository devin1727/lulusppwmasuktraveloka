from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from .forms import schedule_form
from .models import data
def submit(request):
	form = schedule_form(request.POST)
	if request.method == 'POST':
		if form.is_valid():
			response = {}
			response['hari'] = form.cleaned_data.get('hari')
			response['waktu'] = form.cleaned_data.get('waktu')
			response['nama_kegiatan'] = form.cleaned_data.get('nama_kegiatan')
			response['kategori'] = form.cleaned_data.get('kategori')
			yang_mau_disave = data(
				hari = response['hari'],
				waktu =response['waktu'],
				nama_kegiatan =response['nama_kegiatan'],
				kategori =response['kategori'])
			yang_mau_disave.save()
			return render(request,'index.html',{'response':response})
	else :
		form = schedule_form()
		return render(request,'index.html', {'schedule_form' : form})
	return HttpResponseRedirect('show')

def show(request):
	schedule = data.objects.all()
	return render(request,'show.html',{'schedule' : schedule})
